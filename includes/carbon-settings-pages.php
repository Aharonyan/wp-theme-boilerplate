<?php

use Carbon_Fields\Container;
use Carbon_Fields\Field;

// Default options page
$basic_options_container = Container::make('theme_options', __('Theme Options', 'atheme'))
    ->set_page_file('crb_atheme_options')
    ->add_fields(array(
        Field::make('text', 'crb_website_name', __('Website Name', 'atheme'))->set_attribute('placeholder', 'Bevakio'),
        Field::make('header_scripts', 'crb_header_script', __('Header Script', 'atheme')),
        Field::make('footer_scripts', 'crb_footer_script', __('Footer Script', 'atheme')),
    ));
