<?php

use Carbon_Fields\Block;
use Carbon_Fields\Field;


/**
 * Block example
 */
// Block::make(__('Hero Block'))
//     ->add_fields(array(
//         Field::make('text', 'title', __('Block Heading'))->set_width(50),
//         Field::make('textarea', 'description', __('Block Content'))->set_rows(3)->set_width(50),
//         Field::make('image', 'image', __('Block Image'))->set_width(30),
//         Field::make('text', 'button_title', __('Button Title'))->set_width(30),
//         Field::make('text', 'button_url', __('Button Link'))->set_width(30),
//         Field::make('separator', 'crb_separator', '- - - -')
//     ))
//     ->set_category('widgets')
//     //->set_icon( 'heart' )
//     ->set_keywords([__('hero'), __('block'), __('hero block')])
//     //->set_editor_style( 'a-theme-tw-style-editor' )
//     ->set_render_callback(function ($fields, $attributes, $inner_blocks) {
//         require get_template_directory() . '/templates/blocks/hero-block.php';
//     });
