<?php



add_action('carbon_fields_register_fields', 'crb_attach_theme_options');
function crb_attach_theme_options()
{
    require get_template_directory() . '/includes/carbon-settings-pages.php';
}



add_action('after_setup_theme', 'crb_load');
function crb_load()
{
    require_once(get_template_directory() . '/vendor/autoload.php');
    \Carbon_Fields\Carbon_Fields::boot();

    require get_template_directory() . '/includes/carbon-blocks.php';
}
