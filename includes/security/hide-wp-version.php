<?php
/**
 *  Hiding WP version from being displayed in html.
 *
 * @package atheme
 */

remove_action( 'wp_head', 'wp_generator' );
