<?php
/**
 * Generic page for Gutenberg blocks.
 *
 * @package atheme
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

get_header();

the_post();
?>
<section class="container">
	<?php the_content(); ?>
</section>
<?php
get_footer();
