<?php
/**
 *  Blog archive
 *
 * @package atheme
 */

get_header(); ?>

<div class="container">
	<?php
	$sticky_posts = get_option( 'sticky_posts' );

	$args = array(
		'posts_per_page' => -1,
		'post__in'       => $sticky_posts,
	);

	$q = new WP_Query( $args );

	if ( ! empty( $sticky_posts ) && $q->have_posts() && ! is_archive() ) :
		?>
		<div class="sticky">
		<?php
		while ( $q->have_posts() ) :
			$q->the_post();
			?>
			<?php get_template_part( 'partials/post' ); ?>
			<?php
		endwhile;
		?>
		</div>
		<?php
	endif;
	wp_reset_postdata();
	?>

	<div class="row">
		<div class="col-lg-2">
			<ul class="categogy-list">
				<?php
				$args_cat = array(
					'separator'          => '',
					'show_option_all'    => 'All Content',
					'style'              => 'list',
					'title_li'           => '',
					'use_desc_for_title' => false,
				);
				?>
				<?php wp_list_categories( $args_cat ); ?>
			</ul>
		</div>
		<div class="col-lg-10" id="blogposts">
			<?php
			if ( have_posts() ) :

				$i = 0;

				while ( have_posts() ) :
					if ( ! is_sticky() ) {
						$i++;
					}
					if ( 4 === $i && ! is_archive() ) {
						get_template_part( 'partials/subscribe' );
					}
					the_post();
					?>

					<?php
					if ( is_archive() ) {
						get_template_part( 'partials/post' );
					} else {
						if ( ! is_sticky() ) {
							get_template_part( 'partials/post' );
						}
					}
					?>

					<?php
				endwhile;
			endif;
			?>
		</div>
	</div>

	<?php if ( $wp_query->max_num_pages > 1 ) : ?>
		<?php
		$nonce = wp_create_nonce( 'ajax-nonce' );

		$category_object = get_queried_object();
		$category_id     = 0;
		if ( $category_object instanceof WP_Term && 'category' === $category_object->taxonomy ) {
			$category_id = $category_object->term_id;
		}
		?>

		<div class="more-posts">
			<a id="loadmore" class="btn" href="#" data-max-page="<?php echo esc_attr( $wp_query->max_num_pages ); ?>" data-current-page="<?php echo ( get_query_var( 'paged' ) ) ? esc_attr( get_query_var( 'paged' ) ) : 1; ?>" data-category="<?php echo esc_attr( $category_id ); ?>" data-nonce="<?php echo esc_attr( $nonce ); ?>">Load more</a>
		</div>
	<?php endif; ?>
</div>

<?php get_footer(); ?>
