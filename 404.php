<?php
/**
 *  404 page template
 *
 * @package atheme
 */

get_header(); ?>

<?php
$title404 = get_field( 'title_404', 'option' ) ? get_field( 'title_404', 'option' ) : 'Page Not Found';
$button   = get_field( 'button_404', 'option' ) ? get_field( 'button_404', 'option' ) : '';
?>
<div class="">
	<div class="container">
		<div class="error-404">
				<?php if ( get_field( 'logo_404', 'option' ) ) { ?>
					<div class="error-404__logo">
						<img src="<?php the_field( 'logo_404', 'option' ); ?>" alt="logo" />
					</div>
				<?php } ?>
			<div class="error-404__txt">
				<h1 class="error-404__heading"><?php echo esc_html( $title404 ); ?></h1>

				<?php if ( get_field( 'copy_404', 'option' ) ) { ?>
					<div class="error-404__copy"><?php the_field( 'copy_404', 'option' ); ?></div>
				<?php } ?>

				<?php if ( get_field( 'button_404', 'option' ) ) { ?>
					<p class="error-404__back-link"><a class="btn" href="<?php echo esc_url( $button['url'] ); ?>"><?php echo esc_html( $button['title'] ); ?></a><p>
				<?php } else { ?>
					<p class="error-404__back-link"><a class="btn" href="/">Go Back to Homepage</a><p>
				<?php } ?>
			</div>
		</div>
	</div>
</div>

<?php get_footer(); ?>
