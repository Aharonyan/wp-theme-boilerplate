<?php

/**
 *  Main Theme Functions - link and implement all theme specific functionality here.
 *
 * @package a-theme
 */

/**
 * We include modules of theme, order matters!
 */
require_once __DIR__ . '/includes/carbon-load.php';
require_once __DIR__ . '/includes/security/disable-public-rest.php';
require_once __DIR__ . '/includes/security/password-protected.php';
require_once __DIR__ . '/includes/security/hide-wp-version.php';

/**
 * Returns version string for assets.
 *
 * @return null|string
 */
function _get_asset_version()
{
	$main_js_asset = include get_template_directory() . '/assets/js/main.asset.php';

	return $main_js_asset['version'];
}


/**
 * Returns a google fonts url to be used in stylesheets
 *
 * @return string
 */
function _get_fonts_loading_url()
{

	// TODO fill in here your font's URL.
	return 'https://fonts.googleapis.com/css2?family=Open+Sans:wght@400;500;600;700&family=Public+Sans:wght@400;500;600;700&display=swap';
}

/**
 * Theme setup hooks
 */
function chamfr_after_setup_theme()
{

	if (!defined('WP_DEBUG') || (defined('WP_DEBUG') && !WP_DEBUG)) {
		show_admin_bar(false);
	}

	add_theme_support('title-tag');

	add_theme_support('post-thumbnails');

	add_theme_support('woocommerce');

	add_theme_support('wc-product-gallery-zoom');
	add_theme_support('wc-product-gallery-lightbox');
	add_theme_support('wc-product-gallery-slider');

	add_theme_support('wp-block-styles'); // required for blocks editor styles in Gutenberg.

	register_nav_menus(
		array(
			'primary'     => __('Main Menu', 'atheme'),
			'footer_menu' => __('Footer Navigation', 'atheme'),
			'terms_menu'  => __('Footer Terms', 'atheme'),
			'user_menu'   => __('Account Menu', 'atheme'),
		)
	);

	add_theme_support('widgets');
	register_sidebar(
		array(
			'name'          => __('Catalog Filters', 'atheme'),
			'id'            => 'catalog-filters',
			'description'   => '',
			'class'         => '',
			'before_widget' => '<div id="%1$s" class="widget %2$s">',
			'after_widget'  => "</div>\n",
			'before_title'  => '<div class="caption widgettitle">',
			'after_title'   => '<a href="#" class="toggle"></a></div>',
		)
	);

	add_editor_style(
		array(
			get_template_directory_uri() . '/assets/css/editor-style.css?ver=' . _get_asset_version(),
			_get_fonts_loading_url(),
		)
	);
}

add_action('after_setup_theme', 'chamfr_after_setup_theme');


/**
 * Theme scripts and styles
 */
function at_enqueue_scripts()
{

	$asset_version = _get_asset_version();
	$main_js_asset = include get_template_directory() . '/assets/js/main.asset.php';

	wp_enqueue_style('fontawesome', get_template_directory_uri() . '/assets/fonts/fontawesome/css/fontawesome-all.min.css', null, $asset_version);

	wp_enqueue_style('googlefonts', _get_fonts_loading_url(), null, $asset_version);
	wp_enqueue_style('a-theme-tw-style', get_template_directory_uri() . '/assets/css/tailwind.min.css', null, $asset_version);
	wp_enqueue_style('a-theme-style', get_template_directory_uri() . '/assets/css/main.css', array('a-theme-tw-style'), $asset_version);

	wp_register_script(
		'a-theme-scripts',
		get_template_directory_uri() . '/assets/js/main.js',
		$main_js_asset['dependencies'],
		$main_js_asset['version'],
		true
	);

	wp_localize_script('a-theme-scripts', 'main_data', [
		'ajax_url' => admin_url('admin-ajax.php')
	]);

	wp_enqueue_script('a-theme-scripts');
}
add_action('wp_enqueue_scripts', 'at_enqueue_scripts', 20);


/**
 * Enqueue block editor style for Gutenberg Blocks
 */
function at_block_editor_styles()
{
	$asset_version = _get_asset_version();

	wp_enqueue_style('googlefonts', _get_fonts_loading_url(), null, $asset_version);
	wp_enqueue_style('at-blocks-editor-style', get_theme_file_uri('/assets/css/blocks-editor-style.css'), false, $asset_version, 'all');
	wp_enqueue_style('a-theme-tw-style-editor', get_template_directory_uri() . '/assets/css/tailwind.min.css');
}

add_action('enqueue_block_editor_assets', 'at_block_editor_styles');


