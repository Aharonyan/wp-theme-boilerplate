/* Configuration of entry points and paths for all tasks */
const config = {
    wpScripts: {
        main: {
            src: 'src/js/main.js',
            dest: 'assets/js',
        },
        editor: {
            src: 'src/js/editor.js',
            dest: 'assets/js',
        },
    },
    styles: {
        main: {
            src: 'src/scss/main.scss',
            dest: 'assets/css',
            file: 'main.css',
            watchSrc: 'src/scss/**/*.scss',
        },
        // bootstrap: {
        //     src: 'src/scss/bootstrap.scss',
        //     dest: 'assets/css',
        //     file: 'bootstrap.css',
        //     watchSrc: 'src/scss/**/*.scss',
        // },
        editor: {
            src: 'src/scss/editor-style.scss',
            dest: 'assets/css',
            file: 'editor-style.css',
            watchSrc: 'src/scss/**/*.scss',
        },
        blocks: {
            src: 'src/scss/blocks-editor-style.scss',
            dest: 'assets/css',
            file: 'blocks-editor-style.css',
            watchSrc: 'src/scss/**/*.scss',
        },
    },
};

const gulp = require( 'gulp' );
const plumber = require( 'gulp-plumber' );
const concat = require( 'gulp-concat' );
const sourcemaps = require( 'gulp-sourcemaps' );
const sassGlob = require( 'gulp-sass-glob' );
const sass = require( 'gulp-sass' );
const postcss = require( 'gulp-postcss' );
const cssnano = require( 'cssnano' );
const autoprefixer = require( 'autoprefixer' );
const gulpRun = require( 'gulp-run-command' );

const sassTildeImporter = ( url ) => {
    return url[ 0 ] === '~' ? { file: url.substr( 1 ) } : null;
};

const registerStyleTasks = ( configSection ) => {
    // we generate normal styles tasks.
    const tasks = Object.keys( configSection ).map( ( entryKey ) => {
        const currentConfig = configSection[ entryKey ];

        return gulp.task( `style:${ entryKey }`, () => {
            return gulp
              .src( currentConfig.src )
              .pipe( sourcemaps.init() )
              .pipe(
                plumber( {
                    errorHandler: function( err ) {
                        console.log( err );
                        this.emit( 'end' );
                    },
                } )
              )
              .pipe( sassGlob() )
              .pipe(
                sass( {
                    includePaths: [ 'node_modules' ],
                    importer: sassTildeImporter,
                } )
              )
              .pipe( concat( currentConfig.file ) )
              .pipe( postcss( [ autoprefixer, cssnano ] ) )
              .pipe( sourcemaps.write( '.' ) )
              .pipe( gulp.dest( currentConfig.dest ) );
        } );
    } );

    // now we generate watcher tasks.
    const watchTasks = Object.keys( configSection ).map( ( entryKey ) =>
      gulp.task( `style:watch:${ entryKey }`, () =>
        gulp.watch(
          configSection[ entryKey ].watchSrc,
          gulp.series( `style:${ entryKey }` )
        )
      )
    );

    // we return combined build and watch tasks as array.
    return [ ...tasks, watchTasks ];
};

registerStyleTasks( config.styles );

const registerWpScriptsTasks = ( configSection ) => {
    return Object.keys( configSection ).map( ( entryKey ) => {
        const currentConfig = configSection[ entryKey ];
        return [
            gulp.task(
              `js:${ entryKey }`,
              gulpRun.default(
                `npx wp-scripts build ${ currentConfig.src } --output-path=./${ currentConfig.dest } `
              )
            ),
            gulp.task(
              `js:watch:${ entryKey }`,
              gulpRun.default(
                `npx wp-scripts start ${ currentConfig.src } --output-path=./${ currentConfig.dest } `
              )
            ),
        ];
    } );
};
registerWpScriptsTasks( config.wpScripts );

gulp.task(
  'watch',
  gulp.series(
    // gulp.parallel( Object.keys(config.styles).map(key=>`style:${key}`) ),
    // gulp.parallel(Object.keys(config.styles).map(key => `style:watch:${key}`)),
    gulp.parallel( [
        ...Object.keys( config.styles ).map(
          ( key ) => `style:watch:${ key }`
        ),
        ...Object.keys( config.wpScripts ).map(
          ( key ) => `js:watch:${ key }`
        ),
    ] )
  )
);

gulp.task(
  'default',
  gulp.parallel( [
      ...Object.keys( config.styles ).map( ( key ) => `style:${ key }` ),
      ...Object.keys( config.wpScripts ).map( ( key ) => `js:${ key }` ),
  ] )
);
