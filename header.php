<?php

/**
 * Header template for theme.
 *
 * @package atheme
 */

?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>

<head>
	<meta charset="<?php bloginfo('charset'); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
	<?php do_action('body_top'); ?>

	<header id="masthead" class="page-header">
		<?php //get_template_part('/templates/header-nav') ?>

	</header>

	<div id="page" class="site">
		<div class="site-content-contain">
			<div id="content" class="site-content">