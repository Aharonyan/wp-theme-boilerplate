( ( $ ) => {
	'use strict';

	$( '#recalculate-products' ).click( function() {
		const ajaxurl = '/wp-admin/admin-ajax.php';

		const ajaxdata = {
			action: 'calculateprod',
		};

		$( '#prod-calc-spinner' ).css( 'visibility', 'visible' );

		$.ajax( {
			url: ajaxurl,
			data: ajaxdata,
			type: 'POST',
			success( data ) {
				// eslint-disable-next-line prettier/prettier
				$( '#prod-calc' ).html( data + '' );
				$( '#prod-calc-spinner' ).css( 'visibility', 'hidden' );
			},
			error() {
				// eslint-disable-next-line prettier/prettier
				$( '#prod-calc' ).html( '<p>Something went wrong. Try again later.</p>' );
				$( '#prod-calc-spinner' ).css( 'visibility', 'hidden' );
			},
		} );
	} );
} )( jQuery );
